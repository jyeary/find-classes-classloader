/*
 * Copyright 2012-2013 Blue Lotus Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*
 * $Id$
 */
package com.bluelotussoftware.example;

import com.bluelotussoftware.instrumentation.agent.InstrumentationAgent;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * 
 * @author John Yeary <jyeary@bluelotussoftware.com>
 * @version 1.0
 */
@SuppressWarnings("UseOfObsoleteCollectionType")
public class App {

    static {
        InstrumentationAgent.initialize();
    }

    public static void main(String[] args) throws ClassNotFoundException {
        App app = new App();
        
        Class[] all = InstrumentationAgent.getAllLoadedClasses();
        System.out.println("all length -> " + all.length);


        Class[] system = InstrumentationAgent.getSystemClassLoaderInitiatedClasses();
        System.out.println("system length -> " + system.length);


        Class[] appLoader = InstrumentationAgent.getClassLoaderInitiatedClasses(App.class.getClassLoader());
        System.out.println("appLoader length -> " + appLoader.length);


        List<String> classes = app.getLoadedClasses(ClassLoader.getSystemClassLoader());
        System.out.println("classes size -> " + classes.size());

        for (String s : classes) {
            System.out.println(s);
        }
    }

    public List<String> getLoadedClasses(final ClassLoader classLoader) {
        List<String> classNames = null;
        try {
            Field f = ClassLoader.class.getDeclaredField("classes");
            f.setAccessible(true);
            List<Class> classes = new ArrayList<>((Vector<Class>) f.get(classLoader));
            classNames = new ArrayList<>(classes.size());
            for (Class c : classes) {
                classNames.add(c.getCanonicalName());
            }
            return classNames;
        } catch (IllegalArgumentException | IllegalAccessException | NoSuchFieldException | SecurityException ex) {
            Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
        }
        return classNames;
    }
}
